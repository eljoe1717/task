<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Get Messages</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
<h1 id="loading" style="text-align: center">Loading .....</h1>
<div class="row" style="margin: 20px">
    <div class="col-sm-8">
        <div  id="map" style="width: 100%;height: 500px;margin: auto"></div>
    </div>
    <div class="col-sm-4 alerts" style="overflow:scroll; height:500px">

    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAiOgBoR0bz-6mj23O0Wmk1CScy41joFMY&callback=initMap" async defer></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 2,
            center: new google.maps.LatLng(32.222, 31.1111),
        });
        $.ajax({
            url:'/places',
            beforeSend : function () {
                $("#loading").show();
            },
            success : function (data) {
                $("#loading").hide();
                var infowindow = new google.maps.InfoWindow();

                var marker;
                var arr = {
                    Negative : "{{asset('icons/n2.png')}}",
                    Positive : "{{asset('icons/n3.png')}}",
                    Neutral : "{{asset('icons/n1.png')}}"
                };
                var alert = {
                    Negative : "warning",
                    Positive : "success",
                    Neutral : "default"
                };
                Object.keys(data).map(function(key, index) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(data[key].latLng),
                        // icon : arr[data[key].sentiment],
                        map: map
                    });

                    $('.alerts').append(`
                                <div class="alert alert-${alert[data[key].sentiment]}" >
                                        <img src="${arr[data[key].sentiment]}" width="20" height="20">
                                        ${data[key].message}
                                </div>
                            `);

                    google.maps.event.addListener(marker, 'click', (function(marker) {
                        return function() {
                            infowindow.setContent(data[key].message);
                            infowindow.open(map, marker);
                            $('.alerts').empty().append(`
                                <div class="alert alert-info" >
                                    <div class="col-sm-4">
                                        <img src="${arr[data[key].sentiment]}" width="20" height="20">
                                    </div>
                                        ${data[key].message}
                                </div>
                            `);
                        }
                    })(marker));
                });
            }
        });
    }


</script>
</body>
</html>
