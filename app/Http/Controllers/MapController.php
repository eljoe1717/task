<?php

namespace App\Http\Controllers;

use App\Http\Traits\GetContentTrait;
use Illuminate\Http\Request;

class MapController extends Controller
{
    use GetContentTrait;
    public function index()
    {
        $data = file_get_contents('https://spreadsheets.google.com/feeds/list/0Ai2EnLApq68edEVRNU0xdW9QX1BqQXhHRl9sWDNfQXc/od6/public/basic?alt=json');
        $data = json_decode($data)->feed->entry;
        $t = '$t';
        $collection = [];
        foreach ($data as $d){
            $parse = $d->content->$t;
            $parse = str_replace(', message: ','", "message": "',$parse);
            $parse = str_replace(', sentiment: ','", "sentiment": "',$parse).'"';
            $parse = '{ '.str_replace('messageid: ','"messageid": "',$parse).' }';
            $collection[] = json_decode($parse);
        }
        $newCollect = collect($collection)->map(function ($entry){
            $url ="https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyAiOgBoR0bz-6mj23O0Wmk1CScy41joFMY&address=$entry->message";
            $data = $this->getContent($url);
            if ($data->status == 'OK'){
                return [
                    'latLng'=>collect($data->results[0]->geometry->location)->toArray(),
                    'message'=>$entry->message,
                    'sentiment'=>$entry->sentiment
                ];
            }
            return [];
        })->filter()->all();
        return response()->json($newCollect);
    }
}
