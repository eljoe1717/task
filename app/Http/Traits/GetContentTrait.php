<?php

namespace  App\Http\Traits;

trait GetContentTrait {
    public function getContent($url)
    {
        $newStr = str_replace(' ','%20',$url);
        $data = file_get_contents($newStr);
        return json_decode($data);
    }
}
